package kz.askar.sprite;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


/**
 * Created by Zhakenov on 4/8/2017.
 */

public class DrawView extends SurfaceView implements SurfaceHolder.Callback{

    SurfaceHolder surfaceHolder;
    boolean isRunning = false;

    int width = 0;
    int height = 0;

    Man man = null;
    Bitmap manImage;

    public DrawView(Context context) {
        super(context);
        getHolder().addCallback(this);

        manImage = BitmapFactory.decodeResource(getResources(), R.drawable.sprite);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceHolder = holder;
        isRunning = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(isRunning){
                    Canvas canvas = surfaceHolder.lockCanvas();
                    if(canvas==null) break;
                    long startTime = System.currentTimeMillis();
                    update();
                    draw(canvas);
                    long drawTime = System.currentTimeMillis() - startTime;
                    float fps = 1000/(drawTime==0?1:drawTime);
                    Log.d("fps", fps+"");
                    surfaceHolder.unlockCanvasAndPost(canvas);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        isRunning = false;
    }

    public void update(){
        if(man!=null){
            man.move(width, height);
        }
    }

    public void draw(Canvas canvas){
        if(width==0) width = canvas.getWidth();
        if(height==0) height = canvas.getHeight();
        if(man==null) man = new Man(width/10, (width/10)*525/364, manImage);

        canvas.drawColor(Color.WHITE);

        Paint p = new Paint();
        p.setColor(Color.BLACK);
        p.setTextSize(200);

//        canvas.drawText(man.getWidth()+" "+man.getHeight(), 500, 300, p);
        canvas.drawBitmap(man.image, man.getSrcRect(), man.getDestRect(), null);

    }
}
