package kz.askar.sprite;

import android.graphics.Bitmap;
import android.graphics.Rect;

/**
 * Created by Zhakenov on 4/16/2017.
 */

public class Man {

    public int x = 0;
    public int y = 300;
    public int width;
    public int height;
    public int speed = 30;

    public Bitmap image;
    public final int frameWidth = 364;
    public final int frameHeight = 525;
    public int currentFrame = 0;
    public final int movingFrameCount = 6;

    public int direction = 1;



    public Man(int width, int height, Bitmap image){
        this.width = width;
        this.height = height;
        this.image = image;
    }

    public void move(int screenWidth, int screenHeight){

        x+=speed*direction;
        currentFrame = (currentFrame+1)%movingFrameCount;

        if(x<0){
            x = 0;
            direction = 1;
            currentFrame = 0;
        }

        if(x+width>screenWidth){
            x = screenWidth - width;
            direction = -1;
            currentFrame = 0;
        }
    }

    public Rect getSrcRect(){
        return new Rect(frameWidth*currentFrame, frameHeight*(direction==1?0:1),
                frameWidth*(currentFrame+1), frameHeight*((direction==1?0:1)+1));
    }

    public Rect getDestRect(){
        return new Rect(x, y, x+width, y+height);
    }
}
